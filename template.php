<?php

/**
 * Intercept template variables
 *
 * @param $hook
 *   The name of the theme function being executed
 * @param $vars
 *   A sequential array of variables passed to the theme function.
 */
function _phptemplate_variables($hook, $vars = array()) {
  static $comment_number = 1;
  
  switch ($hook) {
    case 'page':
      //if this is an admin page
      if ((arg(0) == 'admin') || 
          (arg(0) == 'node' && arg(1) == 'add') ||
          (arg(0) == 'node' && arg(2) == 'edit')) {
        $vars['is_admin'] = TRUE;
      }
      
      global $user;
      if ($user->uid == 1) {
        $vars['admin'] = TRUE;
      }
      
      if (arg(0) == 'node' && arg(1) == '87') {
        $vars['m3_home'] = TRUE;
      }
      break;
    case 'comment':
      $vars['comment_number'] = $comment_number++;
      $vars['zebra'] = ($comment_number % 2 == 0) ? 'even' : 'odd';
      $vars['admin_comment'] = $vars['comment']->uid == 1 ? TRUE : FALSE;
      break;
  }
  
  return $vars;
}