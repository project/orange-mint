<div class="comment<?php print ($comment->new) ? ' comment-new' : ''; print ($comment->status == COMMENT_NOT_PUBLISHED) ? ' comment-unpublished' : ''; print ' '.$zebra; if ($admin_comment) print ' admin-comment';?>">
<div class="comment-number"><?php print $comment_number; ?></div>
<?php if ($comment->new) : ?>
  <a id="new"></a>
  <span class="new"><?php print $new ?></span>
<?php endif; ?>

<div class="comment-content">
<h3><?php print $title ?></h3>
  <div class="author"><?php print $submitted ?></div>
  <div class="content"><?php print $content ?></div>
  <div class="links"><?php print $links ?></div>
</div>
</div>