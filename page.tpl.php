<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
	"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php print $language ?>" lang="<?php print $language ?>">

<head>
	<title><?php print $head_title ?></title>
	<meta http-equiv="content-language" content="<?php print $language ?>" />
	<?php 
	  print $head;
	  print $styles; 
	  print $scripts;
	?>
</head>

<body<?php if ($is_admin) print ' class="admin"'; print theme('onload_attribute'); ?>>

<div id="header">
  <div id="logo"><h1 title="<?php print $title?>"><a href="<?php print url(); ?>"><span></span><?php print $title?></a></h1></div>
  <div id="menu-bar" class="clearfix">
    <?php if (isset($primary_links)) { ?><?php print theme('links', $primary_links, array('id' => 'nav')) ?><?php } ?>
    <form action="/search/" method="post" id="search_form" class="search-bar">
   		<input type="text" name="edit[keys]" id="edit-keys" value="<?php if (arg(0) == 'search') print arg(2); else print 'search for...'?>" maxlength="50" class="search_box form-text" onfocus="if (this.value==this.defaultValue){this.value='';}" onblur="if (this.value==''){this.value=this.defaultValue;}" />
  		<input name="op" type="image" src="<?php print base_path() . path_to_theme() ?>/img/zoom.gif" id="submit" value="Go" alt="Go" />
	  	<input type="hidden" name="edit[form_id]" id="edit-form_id" value="search_form" />
	  </form>
	</div>
</div>
	  
<div id="wrapper">
  <div id="content">
        <?php
          if ($breadcrumb != '' && $is_admin) {
            print $breadcrumb;
          }
          
          if ($tabs != '') { 
            print '<div class="tabs">'. $tabs .'</div>';
          }
        
          if ($title != '') {
            print '<h2>'. $title .'</h2>';
          }
  
          if ($help != '') {
            print '<div class="help">'. $help .'</div>';
          }
  
          if ($messages != '') {
            print '<div id="messages">'. $messages .'</div>';
          }
  
          print $content;
        ?>
  </div>
</div>

<div id="footer"><p><?php print $footer_message ?></p></div>

<?php print $closure; ?>

</body>
</html>
