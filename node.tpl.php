<div class="node<?php if ($sticky) { print " sticky"; } ?><?php if (!$status) { print " node-unpublished"; } ?>">
  <?php if ($page == 0): ?>
    <h2><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
  <?php endif; ?>
  
  <?php if ($node->type != 'page'): ?><div class="created"><?php print format_date($node->created); ?></div><?php endif; ?>
  <div class="terms"><?php print $terms ?></div>

  <div class="content">
    <?php print $content ?>
  </div>
<?php if ($links): ?>
  <div class="links"><?php print $links ?></div>
<?php endif; ?>
</div>
